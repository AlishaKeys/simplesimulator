﻿using System.Collections.Generic;

namespace Simulator
{
    public class OnCreateObjectsSignal<T>
    {
        public List<T> ObjectsList;
    }
}