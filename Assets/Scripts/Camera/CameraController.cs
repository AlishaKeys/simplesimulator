﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    public class CameraZoom
    {
        public int Zoom;
    }
    /// <summary>
    /// Класс управления камерой
    /// </summary>
    public partial class CameraController: MonoBehaviour 
    {
        private float angle => 75;

        private float angleX;
        private float angleY;

        private void Update()
        {
            Rotate();
        }

        private void Rotate()
        {
            angleX += Input.GetAxis("Horizontal");
            angleX = Mathf.Clamp(angleX, -angle, angle);
            angleY += Input.GetAxis("Vertical");
            angleY = Mathf.Clamp(angleY, -angle, angle);

            transform.eulerAngles = new Vector3(angleX, angleY);
        }

        public class CameraZoomController : IInitializable, IDisposable
        {
            #region Private fields
            //размер камеры для клетки 
            private float fieldOfViewOneCell => 6;
            //минимальный размер камеры для клетки
            private float minFieldOfViewOneCell => fieldOfViewOneCell;
            //максимальный размер камеры для клетки
            private float maxFieldOfViewOneCell => fieldOfViewOneCell * appSetting.GridSize;

            private readonly SignalBus signalBus;

            private readonly Camera screenCamera;

            [Inject] private readonly AppSetting appSetting;
            #endregion

            #region Contructors
            public CameraZoomController(SignalBus signalBus,
                                       Camera screenCamera)
            {
                this.signalBus = signalBus;
                this.screenCamera = screenCamera;
            }
            #endregion

            #region Public fields
            public void Initialize()
            {
                signalBus.Subscribe<CameraZoom>(Zoom);
            }

            public void Dispose()
            {
                signalBus.Unsubscribe<CameraZoom>(Zoom);
            }
            #endregion

            #region Private fields
            private void Zoom(CameraZoom args)
            {
                screenCamera.fieldOfView += args.Zoom * fieldOfViewOneCell;

                if (screenCamera.fieldOfView <= minFieldOfViewOneCell)
                    screenCamera.fieldOfView = minFieldOfViewOneCell;

                if (screenCamera.fieldOfView >= maxFieldOfViewOneCell)
                    screenCamera.fieldOfView = maxFieldOfViewOneCell;
            }
            #endregion
        }
    }
}
