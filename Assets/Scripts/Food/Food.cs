﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    /// <summary>
    /// Класс описывающий создание еды
    /// </summary>
    public partial class Food : MonoBehaviour
    {
        #region Properties
        public GameObject Target { get; set; }
        public Cell Cell { get; set; }

        public Bot Bot { get; set; }
        #endregion

        #region Private fields
        private ParticleSystem ps;

        [Inject] private readonly SignalBus signalBus;
        #endregion

        #region MonoBehaviour calls
        private void Awake()
        {
            ps = GetComponentInChildren<ParticleSystem>();
        }
        #endregion

        #region Private fields
        private void Reinitialize(Vector3 position)
        {
            SetTransformValues(position);
        }

        private void SetTransformValues(Vector3 position)
        {
            transform.position = position;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.Equals(Target))
            {
                ps?.Play();
                StartCoroutine(FoodDespawn());
            }
        }

        private IEnumerator FoodDespawn()
        {
            yield return new WaitForSeconds(1);
            if (Cell) Cell.IsBusy = false;
            signalBus.Fire(new OnFoodDespawn
            {
                Food = this
            });
        }
        #endregion
    }
}
