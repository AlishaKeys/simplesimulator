﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static Simulator.StaticSettings.UIStaticSettings;

namespace Simulator
{
    public class OnCreateFood
    {
        public Vector3 Position;
        public Cell Cell;
        public GameObject Target;
        public Bot Bot;
    }
    /// <summary>
    /// Класс, отвечающий за пул еды
    /// </summary>
    public class FoodController : IInitializable, IDisposable
    {
        #region Private fields
        private readonly SignalBus signalBus;

        private readonly Food.Pool foodPool;
        private readonly List<Food> foods;
        private readonly List<Vector3> foodPositions;

        [Inject] private readonly AppSetting appSetting;

        private IGameStateCommandsExecutor commandsExecutor;
        #endregion

        #region Public calls
        [Inject]
        public void Init(IGameStateCommandsExecutor commandsExecutor)
        {
            this.commandsExecutor = commandsExecutor;
        }

        public FoodController(SignalBus signalBus,
                                   Food.Pool foodPool)
        {
            this.signalBus = signalBus;
            this.foodPool = foodPool;

            foods = new List<Food>();

            foodPositions = new List<Vector3>();
        }

        public void Initialize()
        {
            signalBus.Subscribe<OnCreateFood>(FoodSpawn);
            signalBus.Subscribe<OnFoodDespawn>(FoodDespawn);
            signalBus.Subscribe<GameStateSave>(AddPositions);
            signalBus.Subscribe<GameStateLoad>(LoadPositions);
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<OnCreateFood>(FoodSpawn);
            signalBus.Unsubscribe<OnFoodDespawn>(FoodDespawn);
            signalBus.Unsubscribe<GameStateSave>(AddPositions);
            signalBus.Unsubscribe<GameStateLoad>(LoadPositions);
        }
        #endregion

        #region Private fields
        private void FoodSpawn(OnCreateFood args)
        {
            var food = foodPool.Spawn(args.Position);
            food.Target = args.Target;
            food.Cell = args.Cell;
            food.Bot = args.Bot;
            food.Bot.Target = food.gameObject;
            foods.Add(food);
            foodPositions.Add(food.transform.position);
        }
        private void FoodDespawn(OnFoodDespawn args)
        {
            foodPool.Despawn(args.Food);
            foodPositions.Remove(args.Food.transform.position);
            foods.Remove(args.Food);
        }

        private void AddPositions()
        {
            var cmd = new AddFoodPositionsCommand(foodPositions);
            commandsExecutor.Execute(cmd);
        }

        private void LoadPositions(GameStateLoad args)
        {
            for (int foodIndex = 0; foodIndex < foods.Count; foodIndex++)
            {
                foods[foodIndex].transform.position = args.GameState.FoodPositions[foodIndex];
            }
        }
        #endregion
    }
}