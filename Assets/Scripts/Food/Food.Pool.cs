﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    public partial class Food
    {
        /// <summary>
        /// Пул еды
        /// </summary>
        public class Pool : MonoMemoryPool<Vector3, Food>
        {
            protected override void Reinitialize(Vector3 position, Food food)
            {
                food.Reinitialize(position);
            }
        }
    }
}
