using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

namespace Simulator
{
    public class AddBotPositionsCommand : IGameStateCommand
    {
        public AddBotPositionsCommand(List<Vector3> botBositions)
        {
            BotPositions = botBositions;
        }

        public void Execute(GameState gameState)
        {
            gameState.BotPositions = BotPositions;
        }

        [JsonProperty("BotPositions")]
        private List<Vector3> BotPositions;
    }

    public class AddFoodPositionsCommand : IGameStateCommand
    {
        public AddFoodPositionsCommand(List<Vector3> foodBositions)
        {
            FoodPositions = foodBositions;
        }

        public void Execute(GameState gameState)
        {
            gameState.FoodPositions = FoodPositions;
        }

        [JsonProperty("FoodPositions")]
        private List<Vector3> FoodPositions;
    }
}