using System;

namespace Simulator
{
    public interface ICommandsExecutor<TState, TCommand>
    where TCommand : ICommand
    {
        event Action<TState> StateUpdated;
        void Execute(TCommand command);
    }

    public interface IGameStateCommandsExecutor : ICommandsExecutor<GameState, IGameStateCommand> { }

    public class DefaultCommandsExecutor : IGameStateCommandsExecutor
    {
        protected readonly IGameStateManager gameStateManager;
        protected Action<GameState> stateUpdated;

        public event Action<GameState> StateUpdated
        {
            add
            {
                stateUpdated += value;
                if (value != null)
                {
                    value(gameStateManager.GameState);
                }
            }
            remove
            {
                stateUpdated -= value;
            }
        }

        public DefaultCommandsExecutor(IGameStateManager gameStateManager)
        {
            this.gameStateManager = gameStateManager;
        }

        public virtual void Execute(IGameStateCommand command)
        {
            command.Execute(gameStateManager.GameState);
            if (stateUpdated != null)
            {
                stateUpdated(gameStateManager.GameState);
            }
        }
    }
}