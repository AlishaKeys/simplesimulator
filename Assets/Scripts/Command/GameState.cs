using System.Collections.Generic;
using UnityEngine;

namespace Simulator
{
    [System.Serializable]
    public class GameState
    {
        public List<Vector3> BotPositions;
        public List<Vector3> FoodPositions;
    }
}