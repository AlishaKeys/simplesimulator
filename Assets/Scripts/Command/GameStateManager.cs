
using System.IO;
using UnityEngine;

namespace Simulator
{
    public interface IGameStateManager
    {
        GameState GameState { get; set; }
        void Load();
        void Save();
    }

    public class LocalGameStateManager : IGameStateManager
    {
        public GameState GameState { get; set; }
        private readonly static string gameStatePath = Path.Combine(Application.persistentDataPath, "gameState.json");

        public virtual void Load()
        {

            if (!File.Exists(gameStatePath))
            {
                return;
            }
            GameState = JsonUtility.FromJson<GameState>(File.ReadAllText(gameStatePath));

            if (GameState == null)
                GameState = new GameState();

        }

        public void Save()
        {
            Debug.Log("Saving game state to " + gameStatePath);
            File.WriteAllText(gameStatePath, JsonUtility.ToJson(GameState));
        }
    }
}