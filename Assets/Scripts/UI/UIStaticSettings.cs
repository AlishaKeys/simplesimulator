﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;
using static Simulator.DynamicSettings.UIGameSettings;

namespace Simulator.StaticSettings
{
    /// <summary>
    /// Класс-контроллер между View с настройками и контейнером
    /// основный настроек приложения
    /// </summary>
    public class UIStaticSettings : MonoBehaviour
    {

        public class GameStateSave
        {
        }

        public class GameStateLoad
        {
            public GameState GameState;
        }

        [Header("Panels")]
        [SerializeField, Tooltip("Панель настроек")] private GameObject PanelSettings;
        [SerializeField, Tooltip("Панель с батонами")] private GameObject PanelMenuButtons;
        [SerializeField, Tooltip("Панель со слайдерами")] private GameObject PanelSliders;

        [Header("Buttons")]
        [Space(10)]
        [SerializeField, Tooltip("Кнопка новой симуляции")] private Button NewSimButton;
        [SerializeField, Tooltip("Кнопка загрузки")] private Button LoadButton;
        [SerializeField, Tooltip("Кнопка сохранить")] private Button SaveButton;

        private IGameStateManager gameStateManager;

        [Inject] private readonly SignalBus signalBus;
        [Inject] private AppSetting appSetting;

        [Inject]
        public void Init(IGameStateManager gameStateManager)
        {
            this.gameStateManager = gameStateManager;
        }

        private void Start()
        {
            PanelSettings?.SetActive(true);
            PanelMenuButtons?.SetActive(true);
            PanelSliders?.SetActive(true);

            NewSimButton?.onClick.AddListener(() => PanelMenuButtons.SetActive(false));
            LoadButton?.onClick.AddListener(OnSettingsLoad);
            SaveButton?.onClick.AddListener(OnSettingsSave);
        }

        public void OnSettingsSave()
        {
            Debug.Log("<color=green>В игре нажата кнопка сохранить</color>");

            signalBus.Fire(new GameStateSave { });

            gameStateManager.Save();
        }

        public void OnSettingsLoad()
        {
            Debug.Log("<color=green>В меню нажата кнопка продолжить</color>");
            PanelMenuButtons?.SetActive(false);
            PanelSliders?.SetActive(false);
            gameStateManager.Load();

            signalBus.Fire(new GameStateLoad
            {
                GameState = gameStateManager.GameState
            });

            signalBus.Fire(new OnSettingsChanged
            {
                AppSetting = appSetting
            });
        }

    }
}
