﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Simulator.DynamicSettings
{
    /// <summary>
    /// Класс-контроллер между View с настройками и контейнером
    /// основный настроек приложения
    /// </summary>
    public class UIGameSettings : MonoBehaviour
    {
        public class OnSettingsChanged
        {
            public AppSetting AppSetting;
        }

        [Header("Sliders")]
        [SerializeField, Tooltip("Слайдер количества ячеек")] private Slider NSlider;
        [SerializeField, Tooltip("Слайдер скорости юнитов")] private Slider VSlider;
        [SerializeField, Tooltip("Слайдер скорости игры")] private Slider SpeedGameSlider;

        [Header("Buttons")]
        [Space(10)]
        [SerializeField, Tooltip("Кнопка погнали")] private Button GoButton;
        [SerializeField, Tooltip("Кнопка зума +")] private Button ZoomPlusButton;
        [SerializeField, Tooltip("Кнопка зума -")] private Button ZoomMinusButton;

        [Header("Panels")]
        [SerializeField, Tooltip("Панель слайдеров")] private GameObject PanelSliders;

        [Inject] private readonly SignalBus signalBus;
        [Inject] private AppSetting appSetting;

        void Start()
        {
            NSlider.onValueChanged.AddListener((c) => NSliderValue(c));
            VSlider.onValueChanged.AddListener((c) => VSliderValue(c));
            SpeedGameSlider.onValueChanged.AddListener((c) => GameSpeedValue(c));

            ZoomPlusButton.onClick.AddListener(() => ZoomPlus());
            ZoomMinusButton.onClick.AddListener(() => ZoomMinus());

            GoButton.onClick.AddListener(() => Go());
        }

        private void NSliderValue(float value)
        {
            appSetting.GridSize = (int)value;
        }

        private void VSliderValue(float value)
        {
            appSetting.Speed = (int)value;
        }

        private void GameSpeedValue(float value)
        {
            appSetting.GameSpeed = (int)value;
        }

        private void ZoomPlus()
        {
            signalBus.Fire(new CameraZoom
            {
                Zoom = -1
            });
        }
        private void ZoomMinus()
        {
            signalBus.Fire(new CameraZoom
            {
                Zoom = 1
            });
        }

        private void Go()
        {
            PanelSliders?.SetActive(false);
            signalBus.Fire(new OnSettingsChanged
            {
                AppSetting = appSetting
            });
        }
    }
}
