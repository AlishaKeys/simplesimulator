﻿using Simulator.StaticSettings;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static Simulator.DynamicSettings.UIGameSettings;

namespace Simulator
{
    public class OnCreateCellSignal 
    {
        public Vector3 Position;
        public bool IsBusy;
    }

    /// <summary>
    /// Класс, отвечающий за пул ячеек
    /// </summary>
    public class CellController : IInitializable, IDisposable
    {
        #region Private fields
        private readonly SignalBus signalBus;

        private readonly Cell.Pool cellPool;
        [Inject] private readonly List<Cell> cells;

        private readonly float offsetX = .5f;
        private readonly float offsetY = 1.5f;

        [Inject] private readonly AppSetting appSetting;
        #endregion

        #region Constructors
        public CellController(SignalBus signalBus,
                                   Cell.Pool cellPool)
        {
            this.signalBus = signalBus;
            this.cellPool = cellPool;

            cells = new List<Cell>();
        }
        #endregion

        #region Public calls
        public void Initialize()
        {
            signalBus.Subscribe<OnSettingsChanged>(CreateCells);
            signalBus.Subscribe<OnCreateCellSignal>(CellSpawn);
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<OnSettingsChanged>(CreateCells);
            signalBus.Unsubscribe<OnCreateCellSignal>(CellSpawn);
        }
        #endregion

        #region Private calls
        private void CreateCells()
        {
            var gridSize = appSetting.GridSize;
            var cellSize = appSetting.CellSize;

            var position = Vector3.zero;

            var firstPos = new Vector2 (-cellSize * gridSize / 2f + offsetX, -cellSize * gridSize / 2f + offsetY);

            for (int row = 0; row < gridSize; row++)
            {
                for (int column = 0; column < gridSize; column++)
                {
                    position.x = firstPos.x + cellSize * row;
                    position.y = firstPos.y + cellSize * column;

                    signalBus.Fire(new OnCreateCellSignal
                    {
                        Position = position,
                        IsBusy = false
                    });
                }
            }

            signalBus.Fire(new OnCreateObjectsSignal<Cell>
            {
                ObjectsList = cells
            });
        }

        private void CellSpawn(OnCreateCellSignal args)
        {
            var cell = cellPool.Spawn(args.Position);
            cells.Add(cell);
        }
        #endregion
    }
}