﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    /// <summary>
    /// Класс описывающий создание ячейки
    /// </summary>
    public partial class Cell : MonoBehaviour
    {
        [Inject] private readonly SignalBus signalBus;

        [Inject] private readonly AppSetting appSetting;

        private void Reinitialize(Vector3 position)
        {
            SetTransformValues(position);
        }

        private void SetTransformValues(Vector3 position)
        {
            transform.position = position;
        }
    }
}
