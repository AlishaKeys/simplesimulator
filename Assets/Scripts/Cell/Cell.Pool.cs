﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    public partial class Cell
    {
        public bool IsBusy { get; set; }
        /// <summary>
        /// Пул ячеек
        /// </summary>
        public class Pool : MonoMemoryPool<Vector3, Cell>
        {
            protected override void Reinitialize(Vector3 position, Cell cell)
            {
                cell.Reinitialize(position);
            }
        }
    }
}
