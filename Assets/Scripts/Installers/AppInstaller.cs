using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

/// <summary>
/// Основной класс cо всеми рантайм DI-контейнером, использующийся в контексте проекта
/// </summary>
public class AppInstaller : MonoInstaller<AppInstaller>
{
    [SerializeField, Tooltip("Префаб ячейки")] private GameObject cellPrefab;
    [SerializeField, Tooltip("Контейнер для ячеек")] private Transform cellParent;

    [Space(10)]
    [SerializeField, Tooltip("Префаб бота")] private GameObject botPrefab;
    [SerializeField, Tooltip("Контейнер для ботов")] private Transform botParent;

    [Space(10)]
    [SerializeField, Tooltip("Префаб еды")] private GameObject foodPrefab;
    [SerializeField, Tooltip("Контейнер для еды")] private Transform foodParent;

    [Space(10)]
    [SerializeField, Tooltip("Камера")] private Camera screenCamera;

    [Inject] private readonly AppSetting appSetting;
    
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);

        Container.Bind<Simulator.StaticSettings.UIStaticSettings>().AsSingle().Lazy();
        Container.Bind<Simulator.IGameStateManager>().To<Simulator.LocalGameStateManager>().AsSingle();
        Container.Bind<Simulator.IGameStateCommandsExecutor>().To<Simulator.DefaultCommandsExecutor>().AsSingle();

        Container.DeclareSignal<Simulator.OnCreateCellSignal>();

        Container.DeclareSignal<Simulator.OnCreateFood>();

        Container.DeclareSignal<Simulator.OnCreateObjectsSignal<Simulator.Cell>>();

        Container.DeclareSignal<Simulator.CameraZoom>();

        Container.DeclareSignal<Simulator.OnFoodDespawn>();

        Container.DeclareSignal<Simulator.StaticSettings.UIStaticSettings.GameStateLoad>();

        Container.DeclareSignal<Simulator.StaticSettings.UIStaticSettings.GameStateSave>();

        Container.DeclareSignal<Simulator.DynamicSettings.UIGameSettings.OnSettingsChanged>();

        Container.BindInterfacesAndSelfTo<Simulator.CameraController.CameraZoomController>()
            .AsSingle()
            .WithArguments(screenCamera)
            .Lazy();

        Container.BindMemoryPool<Simulator.Food, Simulator.Food.Pool>()
            .WithInitialSize(appSetting.BotCount)
            .FromComponentInNewPrefab(foodPrefab)
            .UnderTransform(foodParent);

        Container.BindInterfacesAndSelfTo<Simulator.FoodController>().AsSingle().Lazy();

        Container.BindMemoryPool<Simulator.Bot, Simulator.Bot.Pool>()
            .WithInitialSize(appSetting.BotCount)
            .FromComponentInNewPrefab(botPrefab)
            .UnderTransform(botParent);

        Container.BindInterfacesAndSelfTo<Simulator.BotController>().AsSingle().NonLazy();

        Container.BindMemoryPool<Simulator.Cell, Simulator.Cell.Pool>()
            .WithInitialSize(appSetting.GridSize)
            .FromComponentInNewPrefab(cellPrefab)
            .UnderTransform(cellParent);

        Container.BindInterfacesAndSelfTo<Simulator.CellController>().AsSingle().NonLazy();
    }
}