using UnityEngine;
using Zenject;
/// <summary>
/// Основной класс с DI-контейнером не рантайм настроек
/// </summary>
[CreateAssetMenu(fileName = "AppSettingInstaller", menuName = "Installers/AppSettingInstaller")]
public class AppSettingInstaller : ScriptableObjectInstaller<AppSettingInstaller>
{
    [SerializeField] private AppSetting appSetting;

    public override void InstallBindings()
    {
        Container.BindInstance(appSetting);
    }
}