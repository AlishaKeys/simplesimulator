﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Simulator
{
    public partial class Bot
    {
        /// <summary>
        /// Пул ботиков
        /// </summary>
        public class Pool : MonoMemoryPool<Vector3, Bot>
        {
            protected override void Reinitialize(Vector3 position, Bot bot)
            {
                bot.Reinitialize(position);
            }
        }
    }
}
