﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Linq;
using static Simulator.StaticSettings.UIStaticSettings;

namespace Simulator
{
    /// <summary>
    /// Класс, отвечающий за пул ботов
    /// </summary>
    public class BotController : IInitializable, System.IDisposable
    {
        #region Private fields
        private readonly SignalBus signalBus;

        private readonly Bot.Pool botPool;
        private readonly List<Bot> bots;
        private readonly List<Vector3> botPositions;

        [Inject] private readonly AppSetting appSetting;

        private IGameStateCommandsExecutor commandsExecutor;
        #endregion

        [Inject]
        public void Init(IGameStateCommandsExecutor commandsExecutor)
        {
            this.commandsExecutor = commandsExecutor;
        }

        #region Constructors
        public BotController(SignalBus signalBus,
                                   Bot.Pool botPool)
        {
            this.signalBus = signalBus;
            this.botPool = botPool;

            bots = new List<Bot>();

            botPositions = new List<Vector3>();
        }
        #endregion

        #region Public calls
        public void Initialize()
        {
            signalBus.Subscribe<OnCreateObjectsSignal<Cell>>(BotSpawn);
            signalBus.Subscribe<GameStateSave>(AddPositions);
            signalBus.Subscribe<GameStateLoad>(LoadPositions);
        }

        public void Dispose()
        {
            signalBus.Unsubscribe<OnCreateObjectsSignal<Cell>>(BotSpawn);
            signalBus.Unsubscribe<GameStateSave>(AddPositions);
            signalBus.Unsubscribe<GameStateLoad>(LoadPositions);
        }
        #endregion

        #region Private fields
        private void BotSpawn(OnCreateObjectsSignal<Cell> args)
        {
            List<Cell> rndCells = args.ObjectsList.Distinct().OrderBy(_ => Random.Range(0f, 1f)).Take(appSetting.BotCount).ToList();

            for (int botIndex = 0; botIndex < rndCells.Count; botIndex++)
            {
                var bot = botPool.Spawn(rndCells[botIndex].transform.position);
                bot.Cells = args.ObjectsList;
                bots.Add(bot);
                botPositions.Add(bot.transform.position);
            }
        }

        private void AddPositions()
        {
            var cmd = new AddBotPositionsCommand(botPositions);
            commandsExecutor.Execute(cmd);
        }

        private void LoadPositions(GameStateLoad args)
        {
            for (int botIndex = 0; botIndex < bots.Count; botIndex++)
            {
                bots[botIndex].transform.position = args.GameState.BotPositions[botIndex];
            }
        }
        #endregion
    }
}
