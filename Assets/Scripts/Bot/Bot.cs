﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using Zenject;

namespace Simulator
{
    /// <summary>
    /// Класс, описывающий создание ботов
    /// </summary>
    public partial class Bot : MonoBehaviour
    {
        #region Properties
        public GameObject Target { get; set; }
        public List<Cell> Cells { get; set; }
        #endregion

        #region Private fields

        [Inject] private readonly SignalBus signalBus;

        [Inject] private readonly AppSetting appSetting;

        private LayerMask mask;

        private float length => .7f;
        #endregion

        #region MonoBehaviour calls
        private void Awake()
        {
            mask = LayerMask.GetMask("Bot");
        }

        private void Update()
        {
            if (Target == null)
            {
                CreateTarget();
            }
            else
            {
                RaycastHit hit;

                var direction = Target.transform.position - transform.position;
                direction.Normalize();

                if (!Physics.Raycast(transform.position, direction, out hit, length, mask))
                {
                    transform.position = Vector3.MoveTowards(transform.position, Target.transform.position, Time.deltaTime * appSetting.GameSpeed * appSetting.Speed);
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.Equals(Target))
            {
                transform.position = Target.transform.position;
                StartCoroutine(TargetNull());
            }
        }
        #endregion

        #region Private calls
        private void Reinitialize(Vector3 position)
        {
            SetTransformValues(position);
        }

        private void SetTransformValues(Vector3 position)
        {
            transform.position = position;
        }

        private void CreateTarget()
        {
            var cell = Cells.Find(x => !x.IsBusy && ((x.transform.position - transform.position).sqrMagnitude <= Mathf.Pow(appSetting.MaxDist, 2)));

            signalBus.Fire(new OnCreateFood
            {
                Position = cell ? cell.transform.position : transform.position,
                Target = gameObject,
                Bot = this,
                Cell = cell ? cell : Cells.Find(x => (x.transform.position == transform.position))
            });
            if (cell) cell.IsBusy = true;
        }

        private IEnumerator TargetNull()
        {
            yield return new WaitForSeconds(1.1f);
            Target = null;
        }
        #endregion
    }
}
