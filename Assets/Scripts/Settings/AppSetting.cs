﻿using UnityEngine;

/// <summary>
/// Класс основных настроек приложения
/// </summary>
[CreateAssetMenu(fileName = "AppSetting", menuName = "App/Settings", order = 1)]
public class AppSetting : ScriptableObject
{
    [Header("Cell size")]
    public float CellSize = 1;

    [Space(5)]
    [Header("Grid size")]
    [Range(2, 1000)]
    public int GridSize = 5;

    [Space(5)]
    [Header("Bot speed")]
    [Range(1, 100)]
    public float Speed = 5;
    [Range(0, 1000)]
    public float GameSpeed = 1;
    //максимальное время до еды
    public float TimeToFood = 5;
    //максимальная дистанция от еды
    public float MaxDist => (float)Speed / (float)TimeToFood;

    //количество ботов
    public int BotCount => (GridSize * GridSize) / 2;
}
